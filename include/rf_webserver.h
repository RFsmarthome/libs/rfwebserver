#ifndef _RF_WEBSERVER_H
#define _RF_WEBSERVER_H

extern void webserverInit();
extern void webserverLoop(const String &hostname);

#endif
