#include <Arduino.h>
#include <WiFi.h>

#include "rf_webserver.h"

WiFiServer server(80);

void webserverInit()
{
    server.begin();
}

void webserverLoop(const String &hostname)
{
    WiFiClient client = server.available();
    if(client) {
        boolean currentLineIsBlank = true;
        while(client.connected()) {
            if(client.available()) {
                char c = client.read();
                //Serial.write(c);
                // if you've gotten to the end of the line (received a newline
                // character) and the line is blank, the http request has ended,
                // so you can send a reply
                if(c == '\n' && currentLineIsBlank) {
                    // send a standard http response header
                    client.println("HTTP/1.1 200 OK");
                    client.println("Content-Type: text/html");
                    client.println("Connection: close");
                    client.println();
                    client.println("<!DOCTYPE HTML>");
                    client.println("<html>");

                    client.println("<h1>Info:</h1>");

                    client.print("<p>Hostname: ");
                    client.print(hostname);
                    client.println("</p>");

                    client.print("<p>Project: ");
                    client.print(PROJECT_NAME);
                    client.println("</p>");

                    client.print("<p>GIT tag version: ");
                    client.print(GIT_TAG_VERSION);
                    client.println("</p>");
                    
                    client.println("</html>");
                    break;
                }
                if(c == '\n') {
                    // you're starting a new line
                    currentLineIsBlank = true;
                }  else if(c != '\r') {
                    // you've gotten a character on the current line
                    currentLineIsBlank = false;
                }
            }
        }
    }
}
